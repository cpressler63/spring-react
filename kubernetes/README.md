# Kubernetes Deploy Example
This will show a demonstration of how to deploy a backend service and a frontend that uses that backend API
## SERVER API 
#### DEPLOY
We will use this yml file to create a deployment to the kubernetes cluster.   
***kubernetes/server/deployment.yml***
```yml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: spring-react-qatest-be
spec:
  selector:
    matchLabels:
      run: spring-react-qatest-be
  replicas: 2
  template:
    metadata:
      labels:
        run: spring-react-qatest-be
    spec:
      containers:
      - name: spring-react-qatest-be
        image: "softvisionlvcp/spring-react-qatest-be:latest"
        ports:
        - containerPort: 8180
```


#### Lets look at the current deployments

```bash
% kubectl get deployments
NAME       READY   UP-TO-DATE   AVAILABLE   AGE
```


We can deploy a base nginx image to the cluster in this example. This will service but an empty nginx web server
that we can curl to later.
Deploy the service to the kubernetes cluster with this command
```bash
% kubectl apply -f kubernetes/server/deployment.yml
```
Now lets look at the deployments
```bash
% kubectl get deployments
NAME                     READY   UP-TO-DATE   AVAILABLE   AGE
spring-react-qatest-be   1/1     1            1           5h19m
```

The image is now deployed but is not available outside of the kubernetes cluster until we Expose it with a 
service.

## SERVER API Service 
#### SERVICE
We will use this yml file to create a service to expose the deployment in kubernetes cluster to an external device.   
***kubernetes/server/service.yml***
```yml
apiVersion: v1
kind: Service
metadata:
  name: serverapi   # this MUST match the server name configured on the frontend service in nginx.conf
spec:
  type: LoadBalancer
  selector:
    run: spring-react-qatest-be
  ports:
  - port: 8180
```
Run the following command to install the service on the cluster
```bash
% kubectl apply -f kubernetes/server/service.yml
```


```bash

$ kubectl get services
NAME         TYPE           CLUSTER-IP       EXTERNAL-IP   PORT(S)          AGE
kubernetes   ClusterIP      10.96.0.1        <none>        443/TCP          5d6h
serverapi    LoadBalancer   10.100.238.173   localhost     8180:30125/TCP   4h34m
```

Now your can curl to the service on your localhost

```bash
$ curl localhost:8180/v2/api-docs
```

You can verify the service is available to any other hosts on the same network with a curl those hosts

## Front End Service
#### DEPLOY
We will use this yml file to create a deployment to the kubernetes cluster.   
***kubernetes/frontend/deployment.yml***
```yml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: spring-react-qatest-ui
spec:
  selector:
    matchLabels:
      run: spring-react-qatest-ui
  replicas: 1
  template:
    metadata:
      labels:
        run: spring-react-qatest-ui
    spec:
      containers:
      - name: spring-react-qatest-ui
        image: "softvisionlvcp/spring-react-qatest-ui:latest"
        ports:
        - containerPort: 80  # port that the deployment will expose. since this is nginx backed its port 80
```


#### Lets look at the current deployments

```bash
% kubectl get deployments
NAME                     READY   UP-TO-DATE   AVAILABLE   AGE
spring-react-qatest-be   1/1     1            1           5h19m```

We can now deploy our front end application which is a REACT application running on nginx

Deploy the image to the kubernetes cluster with this command
```bash
% kubectl apply -f kubernetes/frontend/deployment.yml
```
Now lets look at the deployments
```bash
% kubectl get deployments
NAME                     READY   UP-TO-DATE   AVAILABLE   AGE
spring-react-qatest-be   1/1     1            1           5h19m
spring-react-qatest-ui   1/1     1            1           104m
```

The image is now deployed but is not available outside of the kubernetes cluster until we Expose it with a 
service.

## Front End Service 
#### SERVICE
We will use this yml file to create a service to expose the deployment in kubernetes cluster to an external device.   
***kubernetes/server/service.yml***
```yml
apiVersion: v1
kind: Service
metadata:
  name: frontend
spec:
  type: LoadBalancer
  selector:
    run: spring-react-qatest-ui
  ports:
    - protocol: TCP
      port: 9999      # port that the service will be available on externally e.g. localhost:9999
      targetPort: 80  # port that the deployment is running on
```
Run the following command to install the service on the cluster
```bash
% kubectl apply -f kubernetes/server/service.yml
```

```bash

$ kubectl get services
NAME         TYPE           CLUSTER-IP       EXTERNAL-IP   PORT(S)          AGE
frontend     LoadBalancer   10.105.187.117   localhost     9999:31965/TCP   30m
kubernetes   ClusterIP      10.96.0.1        <none>        443/TCP          5d6h
serverapi    LoadBalancer   10.100.238.173   localhost     8180:30125/TCP   4h40m
```

Now your can curl to the service on your localhost. It should aso be available from other hosts on your local
network.

```bash
$ curl localhost:9999
```

#### SERVICE ALTERNATE
We will use this yml file to create a service to expose the deployment in kubernetes cluster to an external device.  
This allows us to have a 2nd service available on a different port to the same application. 
***kubernetes/frontend/service-alt.yml***
```yml
apiVersion: v1
kind: Service
metadata:
  name: frontend-alt
spec:
  type: LoadBalancer
  selector:
    run: spring-react-qatest-ui
  ports:
    - protocol: TCP
      port: 80      # port that the service will be available on externally e.g. localhost:80
      targetPort: 80  # port that the deployment is running on
```
Run the following command to install the service on the cluster
```bash
% kubectl apply -f kubernetes/frontend/service-alt.yml
```

```bash

$ kubectl get services
NAME         TYPE           CLUSTER-IP       EXTERNAL-IP   PORT(S)          AGE
frontend     LoadBalancer   10.105.187.117   localhost     9999:32563/TCP   3h6m
frontend2    LoadBalancer   10.105.184.183   localhost     80:30093/TCP     7m3s
kubernetes   ClusterIP      10.96.0.1        <none>        443/TCP          5d9h
serverapi    LoadBalancer   10.100.238.173   localhost     8180:30125/TCP   7h17m
```

Now your can curl to the service on your localhost. It should aso be available from other hosts on your local
network.

```bash
$ curl localhost
```

#### Misc commands to adjust number of backing containers
```bash
kubectl scale deployments/spring-react-qatest-ui --replicas=1
kubectl scale deployments/spring-react-qatest-be --replicas=1

```