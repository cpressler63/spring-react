package com.softvision.example.springboot.dto.translator;


import com.softvision.example.springboot.dto.EventDto;
import com.softvision.example.springboot.dto.GroupDto;
import com.softvision.example.springboot.model.Event;
import com.softvision.example.springboot.model.Group;
import lombok.extern.slf4j.Slf4j;
import lombok.extern.slf4j.XSlf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Component
@Slf4j
public class GroupTranslator {

    @Autowired
    private EventTranslator eventTranslator;

    public GroupDto toDto(Group group) {
        if (group == null) {
            return null;
        }

        GroupDto dto = new GroupDto();

        dto.setId(group.getId());
        dto.setAddress(group.getAddress());
        dto.setCity(group.getCity());
        dto.setCountry(group.getCountry());
        dto.setName(group.getName());
        dto.setStateOrProvince(group.getStateOrProvince());
        dto.setPostalCode(group.getPostalCode());
        Set<EventDto> events = new HashSet<EventDto>(); //default to an empty set
        if ( group.getEvents() != null &&  !group.getEvents().isEmpty()){
            group.getEvents()
                    .stream()
                    .forEach(x -> events.add(eventTranslator.toDto(x)));
        }
        dto.setEvents(events);

        return dto;
    }

    public Group convertDtoToEntity(GroupDto groupDto) {
        if (groupDto == null) {
            log.trace("convertDtoToEntity:groupDto:NULL");
            return null;
        }
        Group group = new Group();
        group.setId(groupDto.getId());
        group.setName(groupDto.getName());
        group.setAddress(groupDto.getAddress());
        group.setCity(groupDto.getCity());
        group.setStateOrProvince(groupDto.getStateOrProvince());
        group.setCountry(groupDto.getCountry());
        group.setPostalCode(groupDto.getPostalCode());


        Set<Event> events = new HashSet<Event>(); //default to an empty set
        if ( groupDto.getEvents() != null &&  !groupDto.getEvents().isEmpty()) {
            groupDto.getEvents()
                    .stream()
                    .forEach(x -> events.add(eventTranslator.convertDtoToEntity(x)));
        }
        group.setEvents(events);

        return group;
    }

}
