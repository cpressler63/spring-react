package com.softvision.example.springboot.dto;

import com.softvision.example.springboot.dto.validation.ValidZipCodeUS;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import javax.validation.constraints.Size;
import java.util.Set;

@Data
@NoArgsConstructor
public class GroupDto {

    @ApiModelProperty(notes = "The database generated group ID")
    private Long id;
    @NonNull
    @Size(min = 1, max = 30)
    @ApiModelProperty(notes = "The group name")
    private String name;
    @ApiModelProperty(notes = "The group address")
    @Size(min = 2, max = 45)
    private String address;
    @ApiModelProperty(notes = "The group city")
    @Size(min = 2, max = 28)
    private String city;
    @ApiModelProperty(notes = "The group state")
    @Size(min = 2, max = 2)
    private String stateOrProvince;
    @ApiModelProperty(notes = "The group country")
    @Size(min = 2, max = 18)
    private String country;
    @ApiModelProperty(notes = "The group zipcode 12345 OR 12345-1234")
    @ValidZipCodeUS
    private String postalCode;

    private Set<EventDto> events;
}
