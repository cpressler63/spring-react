package com.softvision.example.springboot.dto.translator;


import com.softvision.example.springboot.dto.EventDto;
import com.softvision.example.springboot.model.Event;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.HashSet;

@Component
@Slf4j
public class EventTranslator {

    public EventDto toDto(Event event) {
        if (event == null) {
            return null;
        }

        EventDto dto = new EventDto();
        dto.setId(event.getId());
        dto.setDate(event.getDate());
        dto.setDescription(event.getDescription());
        dto.setTitle(event.getTitle());

        return dto;
    }

    public Event convertDtoToEntity(EventDto eventDto) {
        if (eventDto == null) {
            log.trace("convertDtoToEntity:eventDto:NULL");
            return null;
        }
        Event entity = new Event();
        entity.setId(eventDto.getId());
        entity.setDate(eventDto.getDate());
        entity.setDescription(eventDto.getDescription());
        entity.setTitle(eventDto.getTitle());
        entity.setAttendees(new HashSet<>()); //FIXME leave empty for now

        return entity;
    }

}
