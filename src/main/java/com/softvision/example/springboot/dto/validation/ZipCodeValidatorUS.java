package com.softvision.example.springboot.dto.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ZipCodeValidatorUS implements ConstraintValidator<ValidZipCodeUS, String> {


    private ValidZipCodeUS constraintAnnotation;

    @Override
    public void initialize(ValidZipCodeUS validZipCodeUS) {
        this.constraintAnnotation = validZipCodeUS;
    }


    //private static String regex = "^[A-Z]{1,2}[0-9R][0-9A-Z]? [0-9][ABD-HJLNP-UW-Z]{2}$";
    //US ZIP code (U.S. postal code) allow both the five-digit and nine-digit (called ZIP + 4) formats.
    private static String regex = "^[0-9]{5}(?:-[0-9]{4})?$";
    private static Pattern pattern = Pattern.compile(regex);

    @Override
    public boolean isValid(String zipCode, ConstraintValidatorContext context) {
        // zipcode can be null also
        if ( zipCode == null) {
            return true;
        }
        Matcher matcher = pattern.matcher(zipCode);
        return matcher.matches();
    }
}
