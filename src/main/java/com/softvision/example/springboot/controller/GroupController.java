package com.softvision.example.springboot.controller;

import com.softvision.example.springboot.dto.GroupDto;
import com.softvision.example.springboot.model.Group;
import com.softvision.example.springboot.service.GroupService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collection;
import java.util.Optional;

@RestController
@RequestMapping("/api")
class GroupController {

    private final Logger log = LoggerFactory.getLogger(GroupController.class);
    private GroupService groupService;

    @Autowired
    public GroupController(GroupService groupService) {
        this.groupService = groupService;
    }

    @ApiOperation(value = "View a list of available groups", response = GroupDto.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved groups"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    }
    )
    @GetMapping("/groups")
    Collection<GroupDto> groups() {
        log.info("Request to list groups");
        Collection<GroupDto> groups = groupService.findAll();
        log.info("groups found");
        return groups;
    }

    @ApiOperation(value = "View a group", response = GroupDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved group"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    }
    )
    @GetMapping("/groups/{id}")
    ResponseEntity<?> getGroup(@PathVariable Long id) {
        log.info("Request to find group: {}", id);
        Optional<GroupDto> group = groupService.getGroup(id);
        log.info("Response groupID: {} : group", id, group);
        return group.map(response ->  ResponseEntity.ok().body(response))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @PostMapping("/groups")
    ResponseEntity<GroupDto> createGroup(@Valid @RequestBody GroupDto groupDto) throws URISyntaxException {
        log.info("Request to create group: {}", groupDto);
        GroupDto result = groupService.createGroup(groupDto);
        return ResponseEntity.created(new URI("/api/group/" + result.getId()))
                .body(result);
    }

    @PutMapping("/groups/{id}")
    ResponseEntity<GroupDto> updateGroup(@PathVariable Long id, @Valid @RequestBody GroupDto groupDto) {
        log.info("Request to update group with id:{}, group:", id, groupDto);
        GroupDto result = groupService.updateGroup(groupDto);
        return ResponseEntity.ok().body(result);
    }

    @DeleteMapping("/groups/{id}")
    public ResponseEntity<?> deleteGroup(@PathVariable Long id) {
        log.info("Request to delete group: {}", id);
        groupService.deleteGroup(id);
        return ResponseEntity.ok().build();
    }
}