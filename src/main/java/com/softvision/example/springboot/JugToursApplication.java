package com.softvision.example.springboot;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.core.Ordered;
import org.springframework.web.filter.ForwardedHeaderFilter;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import java.util.Map;

@SpringBootApplication
public class JugToursApplication {
    private static final Logger log = LoggerFactory.getLogger(JugToursApplication.class);

    public static void main(String[] args) {

        Map<String, String> env = System.getenv();
        // Java 8
        env.forEach((k, v) -> JugToursApplication.log.debug(k + ":" + v));

        // Classic way to loop a map
//        for (Map.Entry<String, String> entry : env.entrySet()) {
//            JugToursApplication.log.debug(entry.getKey() + " : " + entry.getValue());
//        }

        SpringApplication.run(JugToursApplication.class, args);
    }

    @Bean
    public FilterRegistrationBean<ForwardedHeaderFilter> forwardedHeaderFilterFilterRegistrationBean() {
        ForwardedHeaderFilter forwardedHeaderFilter = new ForwardedHeaderFilter();
        FilterRegistrationBean<ForwardedHeaderFilter> bean = new FilterRegistrationBean<>(forwardedHeaderFilter);
        bean.setOrder(Ordered.HIGHEST_PRECEDENCE);
        return bean;
    }

}