/*
 * Copyright (C) 2018 Allegiant Travel Company - All Rights Reserved
 *
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */
package com.softvision.example.springboot.exception;

import java.util.Set;

public class MissingHeaderException extends RuntimeException {
    private static final String ERROR_MESSAGE = "Request is missing one or more required headers: ";

    public MissingHeaderException(Set<String> headers) {
        super(ERROR_MESSAGE + String.join(", ", headers));
    }
}

