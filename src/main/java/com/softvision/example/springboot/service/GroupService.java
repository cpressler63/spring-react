package com.softvision.example.springboot.service;

import com.softvision.example.springboot.dto.GroupDto;
import com.softvision.example.springboot.dto.translator.GroupTranslator;
import com.softvision.example.springboot.model.Group;
import com.softvision.example.springboot.repository.GroupRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class GroupService {
    @Autowired
    private GroupRepository groupRepository;

    @Autowired
    private GroupTranslator groupTranslator;

    public Collection<GroupDto> findAll() {
        Collection<Group> groups = groupRepository.findAll();
        Collection<GroupDto> groupDtos ;
        groupDtos = groups.stream().map(x->groupTranslator.toDto(x)).collect(Collectors.toList());
        return groupDtos;
    }

    public Optional<GroupDto> getGroup(Long id) {
        Optional<Group> group = groupRepository.findById(id);
        Optional<GroupDto> groupDto = Optional.empty();
        if ( group.isPresent()) {
            groupDto = Optional.of(groupTranslator.toDto(group.get()));
        }
        return groupDto;
    }

    public GroupDto createGroup( GroupDto groupDto) {

        Group result = groupRepository.save(groupTranslator.convertDtoToEntity(groupDto));
        return groupTranslator.toDto(result);
    }


    public GroupDto updateGroup(GroupDto groupDto) {
        Group group = groupTranslator.convertDtoToEntity(groupDto);
        Group result = groupRepository.save(group);
        return groupTranslator.toDto(result);
    }

    public void deleteGroup(Long id) {
        groupRepository.deleteById(id);
    }

}
