#!/bin/bash

cd /home/chet/deploy
docker-compose -p "demo" -f docker-compose-traefik.yml down
docker-compose -p "demo" -f docker-compose-traefik.yml up -d
