FROM centos:7

ARG MAVEN_VERSION=3.6.2
ARG USER_HOME_DIR="/root"
ARG SHA=d941423d115cd021514bfd06c453658b1b3e39e6240969caf4315ab7119a77299713f14b620fb2571a264f8dff2473d8af3cb47b05acf0036fc2553199a5c1ee
ARG BASE_URL=https://apache.osuosl.org/maven/maven-3/${MAVEN_VERSION}/binaries


RUN yum update -y -q && \
yum install -y -q java-11-openjdk-devel && \
rm -rf /var/cache/yum

RUN mkdir -p /usr/share/maven /usr/share/maven/ref \
  && curl -fsSL -o /tmp/apache-maven.tar.gz ${BASE_URL}/apache-maven-${MAVEN_VERSION}-bin.tar.gz \
  && echo "${SHA}  /tmp/apache-maven.tar.gz" | sha512sum -c - \
  && tar -xzf /tmp/apache-maven.tar.gz -C /usr/share/maven --strip-components=1 \
  && rm -f /tmp/apache-maven.tar.gz \
  && ln -s /usr/share/maven/bin/mvn /usr/bin/mvn

# node stuff
RUN curl -sL https://rpm.nodesource.com/setup_10.x | bash - && \
yum install -y  nodejs  && \
npm install -g newman

ENV MAVEN_HOME /usr/share/maven
ENV MAVEN_CONFIG "$USER_HOME_DIR/.m2"
ENV JAVA_HOME=/usr/lib/jvm/java-11-openjdk
ENV M2_HOME=/usr/share/maven
ENV PATH=${M2_HOME}/bin:${PATH}

RUN yum -y install git && \
yum install -y yum-utils   device-mapper-persistent-data   lvm2  && \
yum-config-manager     --add-repo     https://download.docker.com/linux/centos/docker-ce.repo

RUN yum install -y docker-ce
RUN gpasswd -a root docker  && \
newgrp docker

RUN curl -L "https://github.com/docker/compose/releases/download/1.24.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose && \
chmod +x /usr/local/bin/docker-compose


ADD ./docker/gitlab/init.sh /init.sh
RUN chmod +x /init.sh

CMD ./init.sh
