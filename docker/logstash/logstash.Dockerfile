FROM logstash:7.4.2
RUN rm -f /usr/share/logstash/pipeline/logstash.conf

COPY ./docker/logstash/logstash-json.conf /usr/share/logstash/pipeline/logstash.conf
