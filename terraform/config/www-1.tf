
resource "digitalocean_droplet" "www-1" {
  image = "ubuntu-14-04-x64"
  name = "www-1"
  region = "sfo2"
  size = "1gb"
  private_networking = false
  ssh_keys = [
    "${var.ssh_fingerprint}"
  ]

  connection {
    user = "root"
    type = "ssh"
    host = "${self.ipv4_address}"
    private_key = "${file(var.pvt_key)}"
    timeout = "2m"
  }

  provisioner "remote-exec" {
    inline = [
      "export PATH=$PATH:/usr/bin",
      # install nginx
      "sudo apt-get update",
      "sudo apt-get -y install nginx"
    ]
  }
}